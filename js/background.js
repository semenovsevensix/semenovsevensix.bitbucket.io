'use strict';
const color = '#ffffff11'
const canvas = document.getElementById('wall');
const ctx = canvas.getContext('2d');
const random = (min, max) => (Math.random() * (max - min)) + min;
const x = () => Math.floor(random(0, ctx.canvas.offsetWidth));
const y = () => Math.floor(random(0, ctx.canvas.offsetHeight));
const bodyWidth = () => window.innerWidth;
const bodyHeight = () => window.innerHeight;
const backObj = [];
const createRound = () => {
	let obj = new RoundObj(random(0.1, 0.5), getCoord());
	backObj.push(obj);
};
const createCross = () => {
	let obj = new CrossObj(random(0.1, 0.5), getCoord());
	backObj.push(obj);
};

const drawObj = () => {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	for (let obj of backObj) {
		if (obj instanceof RoundObj) {
			let {x, y} = obj.nextPoint(obj.coord.x, obj.coord.y, Date.now());
			ctx.strokeStyle = obj.outlineColor;
			ctx.lineWidth = obj.outlineWidth;
			ctx.save();
			ctx.beginPath();
			ctx.translate(x, y);
			ctx.clearRect(-obj.width / 2, -obj.width / 2, obj.width / 2, obj.width / 2);
			ctx.arc(obj.coord.x, obj.coord.y, obj.radius, 0, 2 * Math.PI);
			ctx.stroke();
			ctx.restore();
			ctx.closePath();
		} else if (obj instanceof CrossObj) {
			let {x, y} = obj.nextPoint(obj.coord.x, obj.coord.y, Date.now());
			if (obj.speed > 0 && obj.deg === 360) {
				obj.deg = 0;
			} else if (obj.speed < 0 && obj.deg === 0) {
				obj.deg = 360;
			}
			obj.deg += obj.speed;
			ctx.strokeStyle = obj.outlineColor;
			ctx.lineWidth = obj.outlineWidth;
			ctx.save();
			ctx.beginPath();
			ctx.translate(x, y);
			ctx.clearRect(-obj.width / 2, -obj.width / 2, obj.width / 2, obj.width);
			ctx.rotate(obj.deg * Math.PI / 180);
			ctx.clearRect(-obj.width / 2, -obj.width / 2, obj.width / 2, obj.width);
			ctx.moveTo(0, 0);
			ctx.lineTo(obj.width / 2, obj.width / 2);
			ctx.moveTo(0, 0);
			ctx.lineTo(-obj.width / 2, obj.width / 2);
			ctx.moveTo(0, 0);
			ctx.lineTo(obj.width / 2, -obj.width / 2);
			ctx.moveTo(0, 0);
			ctx.lineTo(-obj.width / 2, -obj.width / 2);
			ctx.stroke();
			ctx.restore();
			ctx.closePath();
		}
	}
};

const resizeCanvas = () => {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	canvas.setAttribute('width', bodyWidth());
	canvas.setAttribute('height', bodyHeight());
	drawObj();
};

let resizeTimeout;
const resizeThrottler = () => {
	if (!resizeTimeout) {
		resizeTimeout = setTimeout(function () {
			resizeTimeout = null;
			resizeCanvas();
		}, 66);
	}
};

const init = () => {
	canvas.setAttribute('width', bodyWidth());
	canvas.setAttribute('height', bodyHeight());
	let i = 0;
	let count = random(250, 500);
	while (i < count) {
		createRound();
		createCross();
		i++;
	}
	drawObj();

};
const timeFunction = [
	function nextPoint(x, y, time) {
		return {
			x: x + Math.sin((50 + x + (time / 10)) / 100) * 3,
			y: y + Math.sin((45 + x + (time / 10)) / 100) * 4
		};
	},
	function nextPoint(x, y, time) {
		return {
			x: x + Math.sin((x + (time / 10)) / 100) * 5,
			y: y + Math.sin((10 + x + (time / 10)) / 100) * 2
		};
	}
];

const getCoord = () => {
	return {
		x: x(),
		y: y()
	};
};

class BackgroundObj {
	constructor(size) {
		this.size = size;
		this.outlineColor = color;
		this.nextPoint = timeFunction[Math.floor(random(0, 1))];

	}

	get outlineWidth() {
		return 6 * this.size;
	}

}

class RoundObj extends BackgroundObj {
	constructor(size, coord) {
		super();
		this.size = size;
		this.radius = 12 * size;
		this.coord = coord;
	}
}

class CrossObj extends BackgroundObj {
	constructor(size, coord) {
		super();
		this.size = size;
		this.coord = coord;
		this.width = 20 * this.size;
		this.speed = random(-0.2, 0.2);
		this.deg = Math.floor(random(0, 360));
	}
}

//=================================================
init();
window.addEventListener('resize', resizeThrottler, false);
setInterval(drawObj, 50);
