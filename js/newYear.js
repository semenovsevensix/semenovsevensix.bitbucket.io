const now = new Date(Date.now());
const year = now.getFullYear() + 1;
const date = new Date(year, 0);
const timer = document.getElementsByClassName('timer')[0];
const getDate = () => {
  timer.textContent = `${year} год наступит через ${date - Date.now()} мс`;
  window.requestAnimationFrame(getDate)
};
getDate();