'use strict';

class Vector {
	constructor(x = 0, y = 0) {
		this.x = x;
		this.y = y;
	}

	plus(vector) {
		if (!(vector instanceof Vector)) {
			throw  new Error('Это не обьект класса Vector');
		}
		return new Vector(this.x + vector.x, this.y + vector.y);

	}

	times(multiplier) {
		return new Vector(this.x * multiplier, this.y * multiplier);
	}
}

class Actor {
	constructor(pos = new Vector(0, 0), size = new Vector(1, 1), speed = new Vector(0, 0)) {
		if(
			!(
			(pos instanceof Vector) &&
			(size instanceof Vector) &&
			(speed instanceof Vector)
		)
		) {
			throw  new Error('Это не обьект класса Vector');
		}
		this.pos = pos;
		this.size = size;
		this.speed = speed;
	}

	get left() {
		return this.pos.x;
	}

	get top() {
		return this.pos.y;
	}

	get right() {
		return this.pos.x + this.size.x;
	}

	get bottom() {
		return this.pos.y + this.size.y;

	}

	get type() {
		return 'actor';
	}


	act() {
	}

	isIntersect(actor) {
		if (!(actor instanceof Actor)) {
			throw  new Error('Это не обьект класса Actor');
		}

		return !(this === actor) && (
			this.pos.x < actor.pos.x + actor.size.x &&
			actor.pos.x < this.pos.x + this.size.x &&
			this.pos.y < actor.pos.y + actor.size.y &&
			actor.pos.y < this.pos.y + this.size.y
		)
	}

}

class Level {
	constructor(grid = [], actors = []) {
		this.grid = grid;
		this.actors = actors;
		this.height = this.grid.length;
		this.width = this.grid.reduce((prev, curr) => Math.max(prev, curr.length), 0);
		this.status = null;
		this.finishDelay = 1;
	}

	get player() {
		return this.actors.find(item => (item.type === 'player'));
	}

	isFinished() {
		return this.status !== null && this.finishDelay < 0;
	}

	actorAt(actor) {
		if (!(actor instanceof Actor)) {
			throw new Error('Это не обьект класса Actor');
		}
		return this.actors.find(item => actor.isIntersect(item));

	}


	obstacleAt(position, size) {
		if (!(position instanceof Vector) && !(size instanceof Vector)) {
			throw  new Error('Это не обьект класса Vector');
		}

		return(position.y < 0 || position.x < 0 || position.x + Math.ceil(size.x) > this.width)? 'wall':
		(Math.ceil(position.y + size.y) > this.height)? 'lava':
			[].concat(...(this.grid
				.filter((item,y)=> y < Math.ceil(position.y + size.y)&& position.y < (y + 1))
				.map(item => item
					.filter((item,x) =>x < Math.ceil(position.x + size.x) && position.x < x + 1 && item ))))
				.find(item => item !== undefined)
	}

	removeActor(actor) {
		this.actors = this.actors.filter((item) => item !== actor);
	}

	noMoreActors(type) {
		return !(this.actors.some(item => item.type === type));
	}

	playerTouched(type, actor) {
		if (this.status === null) {
			if (type === 'lava' || type === 'fireball') {
				this.status = 'lost';
			} else if (type === 'coin') { //
				this.removeActor(actor);
				if (this.noMoreActors('coin')) {
					this.status = 'won';
				}
			}
		}
	}
}

class LevelParser {
	constructor(dictionary) {
		this.dictionary = dictionary;
	}

	actorFromSymbol(symbol) {
		return (symbol) ? this.dictionary[symbol] : undefined;
	}

	obstacleFromSymbol(symbol) {
		return (symbol === 'x') ? 'wall' : (symbol === '!') ? 'lava' : undefined;
	}

	createGrid(preGrid) {

		return preGrid
			.map(item => [...item]
				.map(item => (item === 'x') ? 'wall' : (item === '!') ? 'lava' : undefined));
	}

	createActors(preActors) {
		return(preActors.length === 0 || !this.dictionary) ? [] :
			[].concat(...
				(preActors.map((item,y) => [...item]
			.map((item,x) => this.dictionary[item]&&
			this.dictionary[item].prototype instanceof Actor ||
					 this.dictionary[item] === Actor ?
				new this.dictionary[item](new Vector(x, y)):undefined)
			.filter(item => item !== undefined))))

	}

	parse(preLevel) {
		return new Level(this.createGrid(preLevel), this.createActors(preLevel));
	}
}


class Fireball extends Actor {
	constructor(pos = new Vector(), speed = new Vector()) {
		super();
		this.pos = pos;
		this.speed = speed;
		this.size = new Vector(1, 1);
	}

	get type() {
		return 'fireball';
	}


	getNextPosition(time = 1) {
		return this.pos.plus(this.speed.times(time));
	}

	handleObstacle() {

		this.speed = this.speed.times(-1);


	}

	act(time, level) {

		level.obstacleAt(this.getNextPosition(time), this.size) === undefined ?
			this.pos = this.getNextPosition(time) :
			this.handleObstacle();


	}
}

class HorizontalFireball extends Fireball {
	constructor(pos = new Vector()) {
		super();
		this.pos = pos;
		this.speed = new Vector(2, 0);
	}
}

class VerticalFireball extends Fireball {
	constructor(pos = new Vector()) {
		super();
		this.pos = pos;
		this.speed = new Vector(0, 2);
	}
}

class FireRain extends Fireball {
	constructor(pos = new Vector()) {
		super();

		this.bas = pos;
		this.pos = this.bas;
		this.speed = new Vector(0, 3);
	}

	handleObstacle() {
		this.pos = new Vector(this.bas.x, this.bas.y);
		this.speed.times(-1);
	}

}

class Coin extends Actor {
	constructor(pos = new Vector()) {
		super();
		this.bas = new Vector(0.2, 0.1).plus(pos);
		this.pos = this.bas;
		this.size = new Vector(0.6, 0.6);
		this.springSpeed = 8;
		this.springDist = 0.07;
		this.spring = Math.floor(Math.random() * 2 * Math.PI);

	}

	get type() {
		return 'coin';
	}


	updateSpring(time = 1) {
		this.spring += this.springSpeed * time;
	}

	getSpringVector() {
		return new Vector(0, Math.sin(this.spring) * this.springDist);
	}

	getNextPosition(time = 1) {
		this.updateSpring(time);
		return new Vector(this.pos.x, this.bas.y).plus(this.getSpringVector());
	}

	act(time) {
		this.pos = this.getNextPosition(time);
	}
}

class Player extends Actor {
	constructor(pos = new Vector(0, 0)) {
		super();
		this.pos = new Vector(pos.x, pos.y - 0.5);
		this.size = new Vector(0.8, 1.5);
	}

	get type() {
		return 'player';
	}


}

//===================================================


const actorDict = {
	'@': Player,
	'=': HorizontalFireball,
	'|': VerticalFireball,
	'v': FireRain,
	'o': Coin

}

const parser = new LevelParser(actorDict);
loadLevels()
	.then( item => runGame(JSON.parse(item), parser, DOMDisplay)
	.then(() => alert('Вы выиграли приз!')))

