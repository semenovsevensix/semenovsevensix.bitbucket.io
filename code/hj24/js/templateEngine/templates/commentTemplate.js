'use strict';

const commentTemplate = ({ timestamp, message }) => ({
  tag: 'div',
  cls: 'comment',
  content: [
    {
      tag: 'p',
      cls: 'comment__time',
      content: `${new Date(timestamp).toLocaleDateString('RU-ru', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
      })}`,
    },
    {
      tag: 'p',
      cls: 'comment__message',
      content: `${message}`,
    },
  ],
});

export default commentTemplate;
