'use strict';

const commentsFormTemplate = ({ left, top }) => ({
  tag: 'form',
  cls: 'comments__form',
  atr: {
    style: `left:${left}px; top:${top}px;`,
  },
  content: [
    {
      tag: 'span',
      cls: 'comments__marker',
      content: '',
    },
    {
      tag: 'input',
      cls: 'comments__marker-checkbox',
      atr: {
        type: 'checkbox',
      },
      content: '',
    },
    {
      tag: 'div',
      cls: 'comments__body',
      content: '',
    },
  ],
});

export default commentsFormTemplate;
