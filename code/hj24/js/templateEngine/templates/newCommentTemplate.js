'use strict';

const newCommentTemplate = () => [
  {
    tag: 'textarea',
    cls: 'comments__input',
    atr: {
      type: 'text',
      placeholder: 'Напишите ответ...',
    },
    content: '',
  },
  {
    tag: 'input',
    cls: 'comments__close',
    atr: {
      type: 'button',
      value: 'Закрыть',
    },
    content: '',
  },
  {
    tag: 'input',
    cls: 'comments__submit',
    atr: {
      type: 'submit',
      value: 'Отправить',
    },
    content: '',
  },
];

export default newCommentTemplate;
