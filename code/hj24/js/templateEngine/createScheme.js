'use strict';

const createScheme = scheme => {
  if (scheme === undefined || scheme === null || scheme === false) {
    return document.createTextNode('');
  }
  if (typeof scheme === 'string' || typeof scheme === 'number' || scheme === true) {
    return document.createTextNode(scheme);
  }
  if (Array.isArray(scheme)) {
    return scheme.reduce((f, elem) => {
      f.appendChild(createScheme(elem));
      return f;
    }, document.createDocumentFragment());
  }
  const element = document.createElement(scheme.tag || 'div');
  [].concat(scheme.cls || []).forEach(className => element.classList.add(className));
  if (scheme.atr) {
    Object.keys(scheme.atr).forEach(key => element.setAttribute(key, scheme.atr[key]));
  }
  element.appendChild(createScheme(scheme.content));
  return element;
};

export default createScheme;
