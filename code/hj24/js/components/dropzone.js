'use strict';

class Dropzone {
  constructor(config) {
    this.element = config.element;
    this.accept = config.accept;
    this.evtFunc = config.evtFunc;
  }

  init() {
    this.element.addEventListener('dragover', event => {
      event.preventDefault();
    });
    this.element.addEventListener('drop', this.evtFunc);
  }
}

export default Dropzone;
