'use strict';

const error = () => ({
  state: 0,
  errMessage: {
    element: document.getElementsByClassName('error')[0],
    message: document.getElementsByClassName('error__message')[0],
  },
  showMessage(config) {
    this.state = config.code;
    this.errMessage.message.textContent = config.msg;
    this.errMessage.element.style.display = 'inline-block';
  },
  hideMessage() {
    this.errMessage.message.textContent = '';
    this.errMessage.element.style.display = 'none';
  },
});

export default error;
