'use strict';

class CustomizableInputTypeFile {
  constructor(config) {
    this.element = config.element;
    this.accept = config.accept;
    this.evtFunc = config.evtFunc;
  }

  init() {
    this.element.style.position = 'relative';
    let input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.style.setProperty('position', 'absolute');
    input.style.left = '0';
    input.style.top = '0';
    input.style.width = '100%';
    input.style.height = '100%';
    input.style.opacity = '0';
    input.style.cursor = 'pointer';
    input.setAttribute('accept', this.accept.join(','));
    input.addEventListener('change', this.evtFunc);
    this.element.appendChild(input);
    return input;
  }
}

export default CustomizableInputTypeFile;
