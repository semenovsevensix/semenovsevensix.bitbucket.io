'use strict';

const loader = () => ({
  element: document.getElementsByClassName('image-loader')[0],
  show() {
    this.element.style.display = 'inline-block';
  },
  hide() {
    this.element.style.display = 'none';
  },
});

export default loader;
