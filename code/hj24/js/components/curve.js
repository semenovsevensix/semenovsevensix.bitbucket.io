'use strict';

import EventsForArray from './eventsForArray.js';
import debounce from '../helpers/debounce.js';
import anotherThrottle from '../helpers/anotherThrottle.js';

class Curve {
  constructor(
    color,
    brushRadius,
    pushEventFn = () => delete this.coords.pushEventFunc,
    spliceEventFn = () => delete this.coords.spliceEventFunc
  ) {
    this.color = color;
    this.brushRadius = brushRadius;
    this.coords = new EventsForArray({
      pushEventName: 'coordPushEvent',
      spliceEventName: 'cordsSpliceEvent',
      pushEventFunc: debounce(pushEventFn, 1000),
      spliceEventFunc: anotherThrottle(spliceEventFn, 1000),
    }).init();
    this.addPoint = coord => this.coords.push(coord);
    this.removePoint = (start, count) => this.coords.splice(start, count);
  }
}

export default Curve;
