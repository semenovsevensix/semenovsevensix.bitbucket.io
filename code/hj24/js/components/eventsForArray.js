'use strict';

class EventsForArray extends Array {
  constructor(config) {
    super();
    this.pushEventName = config.pushEventName || 'curvesPushEvent';
    this.spliceEventName = config.spliceEventName || 'curvesSpliceEvent';
    this.pushEventFunc = config.pushEventFunc || this.push;
    this.spliceEventFunc = config.spliceEventFunc || this.splice;
  }

  init() {
    Object.defineProperties(this, {
      [this.pushEventName]: {
        enumerable: false,
      },
      [this.spliceEventFunc]: {
        enumerable: false,
      },
      [this.pushEventFunc]: {
        enumerable: false,
      },
      [this.spliceEventName]: {
        enumerable: false,
      },
      pushEvent: {
        enumerable: false,
        value: new CustomEvent(this.pushEventName),
      },
      spliceEvent: {
        enumerable: false,
        value: new CustomEvent(this.spliceEventName),
      },
    });
    document.addEventListener(this.pushEventName, this.pushEventFunc);
    document.addEventListener(this.spliceEventName, this.spliceEventFunc);
    return this;
  }

  removeEvent() {
    document.removeEventListener(this.pushEventName, this.pushEventFunc);
    document.removeEventListener(this.spliceEventName, this.spliceEventFunc);
  }

  push(arg) {
    document.dispatchEvent(this.pushEvent);
    super.push(arg);
    return this;
  }

  splice(start, count) {
    document.dispatchEvent(this.pushEvent);
    super.splice(start, count);
    return this;
  }
  reset() {
    this.length = 0;
    return this;
  }
}

export default EventsForArray;
