'use strict';

import createScheme from '../templateEngine/createScheme.js';
import commentTemplate from '../templateEngine/templates/commentTemplate.js';
import commentsFormTemplate from '../templateEngine/templates/commentsFormTemplate.js';
import newCommentTemplate from '../templateEngine/templates/newCommentTemplate.js';
import imgLoader from '../components/loader.js';

class Comments {
  constructor(appContext, comments = {}) {
    this.app = appContext;
    this.comments = comments;
    this.cleanAll = () => {
      document
        .querySelectorAll('.comments__form')
        .forEach(item => item.parentElement.removeChild(item));
    };
    this.hideOrRemoveEmpty = () =>
      document.querySelectorAll('.comments__body').forEach(item => {
        if (!item.querySelector('.comment')) {
          item.parentElement.parentElement.removeChild(item.parentElement);
        }
        item.style.display = 'none';
      });
    this.markerClickEvent = element => {
      element.querySelector('.comments__marker-checkbox').addEventListener('click', event => {
        let body = event.target.nextSibling;
        if (body.style.display === 'block') {
          body.style.display = 'none';
          body.querySelectorAll('.comments__body .comment').forEach(() => {
            body.parentElement.parentElement.removeChild(body.parentElement);
          });
        } else {
          document.querySelectorAll('.comments__body').forEach(item => {
            item.style.display = 'none';
          });
          body.style.display = 'block';
        }
      });
    };
    this.closeButtonClickEvent = element => {
      element.querySelector('.comments__close').addEventListener('click', event => {
        event.target.parentElement.style.display = 'none';
        event.target.parentElement.parentElement.querySelector(
          '.comments__marker-checkbox'
        ).checked = false;
        if (!event.target.parentElement.parentElement.querySelector('.comments__body .comment')) {
          event.target.parentElement.parentElement.parentElement.removeChild(
            event.target.parentElement.parentElement
          );
        }
      });
    };

    this.submitButtonEventFn = id =>
      function(res) {
        let formId = id;
        let comment =
          res.comments[
            Object.keys(res.comments)
              .reverse()
              .find(key => `${res.comments[key].top}:${res.comments[key].left}` === formId)
          ];
        let commentForm = document.getElementById(`${comment.top}:${comment.left}`);
        let input = commentForm.querySelector('.comments__input');
        let loader = commentForm.querySelector('.loader');
        let body = commentForm.querySelector('.comments__body');
        input.value = null;
        loader.parentElement.removeChild(loader);
        input.style.display = 'block';
        body.insertBefore(createScheme(commentTemplate(comment)), input);
      };
    this.submitButtonEvent = (element, fn) => {
      element.querySelector('.comments__submit').addEventListener('click', event => {
        event.preventDefault();
        let parent = event.target.parentElement;
        let input = event.target.parentElement.querySelector('.comments__input');
        input.style.display = 'none';
        let loader = imgLoader()
          .element.querySelector('.loader')
          .cloneNode(true);
        parent.insertBefore(loader, input);
        const config = {
          id: this.app.image.id,
          message: event.target.parentElement.parentElement.querySelector('.comments__input').value,
          top: parseFloat(event.target.parentElement.parentElement.style.top),
          left: parseFloat(event.target.parentElement.parentElement.style.left),
          fn: fn,
        };
        this.app.serverInt.addComment(config);
      });
    };
  }

  setEvents(form) {
    this.markerClickEvent(form);
    this.closeButtonClickEvent(form);
    this.submitButtonEvent(form, this.submitButtonEventFn(form.id));
  }

  new(config) {
    this.hideOrRemoveEmpty();
    const form = createScheme(commentsFormTemplate(config));
    form.querySelector('.comments__marker-checkbox').checked = true;
    form.id = `${config.top}:${config.left}`;
    const newCommentElement = createScheme(newCommentTemplate());
    const body = form.querySelector('.comments__body');
    body.appendChild(newCommentElement);
    form.style.display = 'block';
    let block = document.createDocumentFragment().appendChild(form);
    this.setEvents(block);
    document.getElementsByClassName('app')[0].appendChild(block);
  }

  init(comments) {
    let obj = {};
    let fragment = document.createDocumentFragment();

    Object.keys(comments).reduce((prev, next) => {
      if (!prev[`${comments[next].top}:${comments[next].left}`]) {
        prev[`${comments[next].top}:${comments[next].left}`] = createScheme(
          commentsFormTemplate({ top: comments[next].top, left: comments[next].left })
        );
        prev[`${comments[next].top}:${comments[next].left}`]
          .querySelector('.comments__body')
          .appendChild(createScheme(newCommentTemplate()));
      }
      let body = prev[`${comments[next].top}:${comments[next].left}`].querySelector(
        '.comments__body'
      );
      let comment = createScheme(
        commentTemplate({ message: comments[next].message, timestamp: comments[next].timestamp })
      );

      body.insertBefore(comment, body.querySelector('.comments__input'));
      return prev;
    }, obj);
    Object.keys(obj).forEach(key => {
      obj[key].id = key;
      fragment.appendChild(obj[key]);
      this.setEvents(obj[key]);
    });
    document.getElementsByClassName('app')[0].appendChild(fragment);
  }
}

export default Comments;
