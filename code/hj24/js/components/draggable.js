'use strict';

import throttle from '../helpers/throttle.js';

class Draggable {
  constructor(element, field, func) {
    this.element = element;
    this.field = field;
    this.func = func;
    this.movedPiece = null;
    this.minY = null;
    this.minX = null;
    this.maxX = null;
    this.maxY = null;
    this.shiftX = 0;
    this.shiftY = 0;
    this.dragStart = event => {
      if (event.target === element) {
        this.movedPiece = event.target.parentElement;
        this.minY = this.field.offsetTop;
        this.minX = this.field.offsetLeft;
        this.maxX = this.field.offsetLeft + this.field.offsetWidth - this.movedPiece.offsetWidth;
        this.maxY = this.field.offsetTop + this.field.offsetHeight - this.movedPiece.offsetHeight;
        this.shiftX = event.pageX - event.target.getBoundingClientRect().left - window.pageXOffset;
        this.shiftY = event.pageY - event.target.getBoundingClientRect().top - window.pageYOffset;
      }
    };

    this.drag = throttle((x, y) => {
      if (this.movedPiece) {
        x = x - this.shiftX;
        y = y - this.shiftY;
        x = Math.min(x, this.maxX);
        y = Math.min(y, this.maxY);
        x = Math.max(x, this.minX);
        y = Math.max(y, this.minY);
        this.func({
          top: y,
          left: x - (this.element.parentElement.getBoundingClientRect().width / 100) * 0.1,
        });
        this.movedPiece.classList.add('moving');
      }
    });
    this.drop = () => {
      if (this.movedPiece) {
        this.movedPiece.classList.remove('moving');
        this.movedPiece = null;
      }
    };
  }

  init() {
    document.addEventListener('mousedown', this.dragStart);
    document.addEventListener('mousemove', event => this.drag(event.pageX, event.pageY));
    document.addEventListener('mouseup', this.drop);

    document.addEventListener('touchstart', event => this.dragStart(event.touches[0]));
    document.addEventListener('touchmove', event =>
      this.drag(event.touches[0].pageX, event.touches[0].pageY)
    );
    document.addEventListener('touchend', event => this.drop(event.changedTouches[0]));
  }
}

export default Draggable;
