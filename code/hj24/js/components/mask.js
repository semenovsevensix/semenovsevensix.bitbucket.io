'use strict';

import EventsForArray from './eventsForArray.js';
import anotherThrottle from '../helpers/anotherThrottle.js';
import Curve from './curve.js';
import throttle from '../helpers/throttle.js';

class Mask {
  constructor(config) {
    this.appContext = config.appContext;
    this.element = document.createElement('canvas');
    this.maskImg = document.createElement('img');
    this.rootElement = document.querySelector(':root');
    this.img = config.element;
    this.app = config.app;
    this.mini = false;
    this.activeColor = config.color;
    this.imgTop = () => `${this.img.getBoundingClientRect().y}px`;
    this.imgLeft = () => `${this.img.getBoundingClientRect().x}px`;
    this.imgHeight = () => `${this.img.getBoundingClientRect().height}`;
    this.imgWidth = () => `${this.img.getBoundingClientRect().width}`;
    this.parrent = this.img.parentElement;
    this.context = this.element.getContext('2d');
    this.context.strokeStyle = this.activeColor;
    this.context.fillStyle = this.activeColor;
    this.currentCurve = null;
    this.curves = new EventsForArray({
      pushEventName: 'curvesPushEvent',
      spliceEventName: 'curvesSpliceEvent',
      pushEventFunc: anotherThrottle(() => this.send(), 1000),
      spliceEventFunc: anotherThrottle(event => event, 1000),
    }).init();
    this.drawing = false;
    this.draw = false;
    this.needsRepaint = false;
    this.circle = point => {
      this.context.beginPath();
      this.context.arc(...point, this.context.brushRadius / 2, 0, 2 * Math.PI);
      this.context.fill();
    };
    this.smoothCurveBetween = (p1, p2) => {
      const cp = p1.map((coord, idx) => (coord + p2[idx]) / 2);
      this.context.quadraticCurveTo(...p1, ...cp);
    };
    this.smoothCurve = points => {
      this.context.beginPath();
      this.context.lineWidth = this.context.brushRadius;
      this.context.lineJoin = 'round';
      this.context.lineCap = 'round';

      this.context.moveTo(...points[0]);
      for (let i = 1; i < points.length - 1; i++) {
        this.smoothCurveBetween(points[i], points[i + 1]);
        this.context.stroke();
      }
    };
    this.makePoint = (x, y) => {
      return [x, y];
    };
    this.repaint = () => {
      this.curves.forEach(curve => {
        this.setColor(curve.color);
        this.setBrushRadius(curve.brushRadius);
        this.circle(curve.coords[0]);

        this.smoothCurve(curve.coords);
      });
    };
    this.tick = () => {
      if (this.needsRepaint) {
        this.repaint();
        this.needsRepaint = false;
      }

      window.requestAnimationFrame(this.tick);
    };
  }

  init() {
    this.context.clearRect(0, 0, this.element.width, this.element.height);
    this.context.brushRadius = 4;
    this.parrent.style.position = 'relative';
    this.element.style.position = 'absolute';
    this.element.style.zIndex = '998';
    this.maskImg.style.position = 'absolute';
    this.maskImg.style.zIndex = '997';
    this.mini = !(
      this.img.naturalWidth > this.img.parentElement.getBoundingClientRect().width ||
      this.img.naturalHeight > this.img.parentElement.getBoundingClientRect().height
    );
    this.element.height = this.img.naturalHeight;
    this.element.width = this.img.naturalWidth;
    this.resize();
    this.positionImg();
    this.parrent.appendChild(this.element);
    this.parrent.appendChild(this.maskImg);

    this.mousedownEvt = evt => {
      this.drawing = true;
      this.currentCurve = new Curve(this.context.strokeStyle, this.context.brushRadius, () => {
        this.send();
        this.curves.reset();
      });
      this.curves.push(this.currentCurve);
      this.currentCurve.addPoint(this.makePoint(evt.offsetX, evt.offsetY));
      this.needsRepaint = true;
    };
    this.mouseupEvt = () => {
      this.drawing = false;
      this.currentCurve.coords.removeEvent();
    };
    this.mousemoveEvt = evt => {
      if (this.drawing) {
        this.currentCurve.addPoint(this.makePoint(evt.offsetX, evt.offsetY));
        this.needsRepaint = true;
      }
    };

    this.mouseEvt = () => (this.drawing = false);
    window.addEventListener(
      'resize',
      throttle(() => {
        this.positionImg();
        this.resize();
      })
    );
    this.tick();
    return this;
  }
  send() {
    this.element.toBlob(blob => this.appContext.serverInt.websocket.send(blob));
  }
  enableDraw() {
    if (!this.draw) {
      this.element.addEventListener('mousedown', this.mousedownEvt);
      this.element.addEventListener('mouseup', this.mouseupEvt);
      this.element.addEventListener('mouseleave', this.mouseEvt);
      this.element.addEventListener('mousemove', this.mousemoveEvt);
      this.draw = true;
    }
  }
  disableDraw() {
    if (this.draw) {
      this.element.removeEventListener('mousedown', this.mousedownEvt);
      this.element.removeEventListener('mouseup', this.mouseupEvt);
      this.element.removeEventListener('mouseleave', this.mouseEvt);
      this.element.removeEventListener('mousemove', this.mousemoveEvt);
      this.draw = false;
    }
  }

  resize() {
    this.element.width = `${this.imgWidth()}`;
    this.element.height = `${this.imgHeight()}`;
    this.element.style.top = `${this.imgTop()}`;
    this.element.style.left = `${this.imgLeft()}`;
    this.maskImg.width = this.imgWidth();
    this.maskImg.height = this.imgHeight();
    this.maskImg.style.top = this.imgTop();
    this.maskImg.style.left = this.imgLeft();
    this.repaint();
  }
  setNaturalSize() {
    this.img.style.width = `${this.img.naturalWidth}px`;
    this.img.style.height = `${this.img.naturalHeight}px`;
    this.resize();
  }
  fillScreen() {
    this.img.style.width = '';
    this.img.style.height = '';
    if (
      this.img.naturalWidth - this.rootElement.offsetWidth >
        this.img.naturalHeight - this.rootElement.offsetHeight &&
      this.img.naturalWidth > this.img.naturalHeight
    ) {
      this.img.style.width = '97%';
    } else {
      this.img.style.height = '97%';
    }

    this.img.style.left = `${(window.innerWidth - this.img.getBoundingClientRect().width) / 2}px`;
    this.img.style.top = '10px';
    this.resize();
  }
  getCenter(obj) {
    return {
      x: obj.getBoundingClientRect().width / 2,
      y: obj.getBoundingClientRect().height / 2,
    };
  }

  shiftCenter() {
    this.img.style.left = `${this.getCenter(this.img.parentElement).x -
      this.getCenter(this.img).x}px`;
    this.img.style.top = `${this.getCenter(this.img.parentElement).y -
      this.getCenter(this.img).y}px`;
  }
  positionImg() {
    if (this.mini) {
      this.setNaturalSize();
      this.resize();
      this.shiftCenter();
    } else {
      this.fillScreen();
    }
  }
  setColor(color) {
    this.activeColor = color;
    this.context.strokeStyle = color;
    this.context.fillStyle = color;
  }

  setBrushRadius(radius) {
    this.context.brushRadius = radius;
  }
}

export default Mask;
