'use strict';
const menuConfig = (app, { shareUrlInput }) => ({
  burger: {
    eventType: 'click',
    event() {
      app.menu.state = 'initial';
      app.mask.element.disableDraw();
      if (app.menu.selectedItem) {
        app.menu.selectedItem.element.dataset.state = '';
        app.menu.selectedItem = {
          name: null,
          element: {
            dataset: {
              state: '',
            },
          },
        };
      }
    },
  },
  comments: {
    eventType: 'click',
    event() {
      app.menu.selectItem('comments');
      app.mask.element.disableDraw();
    },
    tool: {
      eventType: 'click',
      event(event) {
        let elements = document.getElementsByClassName('comments__form');
        if (event.currentTarget.children[0].firstElementChild.checked) {
          for (let item of elements) {
            item.style.display = 'block';
          }
        } else {
          for (let item of elements) {
            item.style.display = 'none';
          }
        }
      },
    },
  },
  draw: {
    eventType: 'click',
    event() {
      app.menu.selectItem('draw');
      app.mask.element.enableDraw();
      app.mask.element.setColor(app.mask.element.activeColor);
    },
    tool: {
      eventType: 'click',
      event() {
        let color = app.mask.element.activeColor;
        switch (event.target.value) {
          case 'blue':
            color = '#53a7f5';
            break;
          case 'red':
            color = '#ea5d56';
            break;
          case 'yellow':
            color = '#f3d135';
            break;
          case 'green':
            color = '#6cbe47';
            break;
          case 'purple':
            color = '#b36ade';
            break;
          default:
            color = '#6cbe47';
        }
        app.mask.element.setColor(color);
      },
    },
  },
  share: {
    eventType: 'click',
    event() {
      app.menu.selectItem('share');
      app.mask.element.disableDraw();
    },
    tool: {
      eventType: 'click',
      event() {
        navigator.clipboard.writeText(shareUrlInput.value).catch(err => {
          app.err.showMessage(err);
        });
      },
    },
  },
});

export default menuConfig;
