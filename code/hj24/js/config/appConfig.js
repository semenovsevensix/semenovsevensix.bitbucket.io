'use strict';

import loader from '../components/loader.js';

const config = {
  get APP_URL() {
    return 'https://netology-code.github.io/hj-24-SemenovAV/';
  },
  get imageElement() {
    return document.getElementsByClassName('current-image')[0];
  },
  get shareUrlInput() {
    return document.getElementsByClassName('menu__url')[0];
  },
  get imgLoader() {
    return loader();
  },
  get PARAMETERS() {
    return { id: window.location.hash.replace('#', '') };
  },
};
export default config;
