'use strict';

import Comments from '../components/comments.js';
import imgLoader from '../components/loader.js';

const serverInt = (app, { shareUrlInput, imageElement, APP_URL }) => ({
  url: 'https://neto-api.herokuapp.com/',
  testErr(res) {
    if (200 <= res.status && res.status < 300) {
      app.err.hideMessage();
      return res;
    }
    throw new Error(res.statusText);
  },
  addImg(file) {
    imgLoader().show();
    document.location.hash = '';
    const data = new FormData();
    data.append('title', file.name);
    data.append('image', file);
    fetch(`${this.url}pic`, {
      method: 'POST',
      body: data,
    })
      .then(this.testErr)
      .then(res => res.json())
      .then(res => {
        document.location.hash = `${res.id}`;
        localStorage.setItem('image', JSON.stringify(res));
        app.image = res;
        imageElement.src = res.url;
        imageElement.id = res.id;
        new Comments(app).cleanAll();
        this.getInfo(res.id, 'share');
      })
      .catch(err => app.err.showMessage({ msg: `Сервер: ${err.message}`, code: 2 }));
  },
  getInfo(id, selected = 'comments') {
    if (!app.image || !app.image.id || app.image.id !== id) {
      imgLoader().show();
    }
    fetch(`${this.url}pic/${id}`)
      .then(this.testErr)
      .then(res => res.json())
      .then(() => {
        imageElement.addEventListener('load', () => app.menu.selectItem(selected));
        app.image = JSON.parse(localStorage.getItem('image'));
        if (app.serverInt.websocket.url !== `${this.websocket.url}${id}`) {
          app.serverInt.websocket.open(app.image.id);
        }
        imageElement.src = app.image.url;
        shareUrlInput.value = `${APP_URL}#${id}`;
        let fn = () => {
          if (app.mask.element.init) {
            app.mask.element.src = app.image.mask;
            app.mask.element.init();
            app.mask.element.resize();
          }
          imageElement.removeEventListener('load', fn);
        };
        imageElement.addEventListener('load', fn);
      })
      .catch(err => app.err.showMessage({ msg: `Сервер: ${err.message}`, code: 2 }));
  },
  addComment(config) {
    const data = `message=${encodeURIComponent(config.message)}&top=${encodeURIComponent(
      config.top
    )}&left=${encodeURIComponent(config.left)}`;
    return fetch(`${this.url}pic/${config.id}/comments`, {
      method: 'POST',
      body: data,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
      .then(this.testErr)
      .then(res => res.json())
      .then(config.fn)
      .catch(err => app.err.showMessage({ msg: `Сервер: ${err.message}`, code: 2 }));
  },
  websocket: {
    url: 'wss://neto-api.herokuapp.com/pic/',
    open(id) {
      this.connection = new WebSocket(`${this.url}${id}`);

      this.connection.addEventListener('open', () => {
        app.err.hideMessage();
      });
      this.connection.addEventListener('message', event => {
        let data = JSON.parse(event.data);
        switch (data.event) {
          case 'mask':
            app.mask.element.maskImg.src = data.url;
            app.image.mask = data.url;
            break;
          case 'pic':
            app.image = data.pic;
            if (data.pic.mask) {
              app.mask.element.maskImg.src = data.pic.mask;
              app.image.mask = data.pic.mask;
            } else if (app.mask.element.maskImg.parentElement) {
              app.mask.element.maskImg.parentElement.removeChild(app.mask.element.maskImg);
            }
            if (data.pic.comments) {
              new Comments(app).init(data.pic.comments);
            }

            break;
          case 'comment':
            if (data.comment) {
              app.serverInt.getInfo(app.image.id);
            }
            break;
        }
      });
      this.connection.addEventListener('close', event => {
        if (event.code !== 1000) {
          app.err.showMessage({
            msg: 'Веб-сокет: соединение закрыто аварийно!',
            code: 3,
          });
        }
      });
      this.connection.addEventListener('error', error => {
        app.err.showMessage({ msg: `Веб-сокет: ${error.data}`, code: 3 });
      });
      window.addEventListener('beforeunload', () => {
        this.connection.onclose = function() {};
        this.connection.close(1000);
      });
    },
    close() {
      if (this.connection) {
        this.connection.close(1000);
      }
    },
    send(data) {
      if (this.connection && this.connection.readyState === 1) {
        this.connection.send(data);
      }
    },
  },
});

export default serverInt;
