'use strict';

import Mask from '../components/mask.js';

const mask = app => ({
  element: new Mask({
    appContext: app,
    element: document.getElementsByClassName('current-image')[0],
    app: document.getElementsByClassName('app')[0],
    color: '#6cbe47',
  }),
});

export default mask;
