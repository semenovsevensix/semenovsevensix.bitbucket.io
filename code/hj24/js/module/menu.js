'use strict';

import Draggable from '../components/draggable.js';
import CustomizableInputTypeFile from '../components/customizableInputTypeFile.js';
import Dropzone from '../components/dropzone.js';
import menuConfig from '../config/menuConfig.js';
import appConfig from '../config/appConfig.js';

const menu = app => ({
  root: document.querySelector(':root'),
  element: document.getElementsByClassName('menu')[0],
  topVarName: '--menu-top',
  leftVarName: '--menu-left',
  item: {},
  get state() {
    return this.element.dataset.state;
  },
  set state(state) {
    this.element.dataset.state = state;
  },
  get width() {
    return () => this.element.getBoundingClientRect().width;
  },

  get height() {
    return () => this.element.getBoundingClientRect().height;
  },

  get position() {
    return () => ({
      top:
        parseFloat(this.root.style.getPropertyValue(this.topVarName)) *
        (this.root.clientHeight / 100),
      left:
        parseFloat(this.root.style.getPropertyValue(this.leftVarName)) *
        (this.root.clientWidth / 100),
    });
  },

  set position(coordinate) {
    this.root.style.setProperty(
      this.topVarName,
      `${coordinate.top / (this.root.clientHeight / 100)}%`
    );
    this.root.style.setProperty(
      this.leftVarName,
      `${coordinate.left / (this.root.clientWidth / 100)}%`
    );
  },

  positionCenter() {
    this.position = {
      top: this.root.clientHeight / 2 - this.height() * 2,
      left: this.root.clientWidth / 2 - this.width() / 2,
    };
  },

  reCountCoords() {
    return () => {
      let coords = this.position();
      this.position = { top: 0, left: 0 };
      if (coords.left + this.width() > this.root.getBoundingClientRect().width) {
        coords.left -= coords.left + this.width() - this.root.getBoundingClientRect().width;
      }
      if (coords.top + this.height() > this.root.getBoundingClientRect().height) {
        coords.top -= coords.top + this.height() - this.root.getBoundingClientRect().height;
      }
      if (coords.left < 0) {
        coords.left = 0;
      }
      if (coords.top < 0) {
        coords.top = 0;
      }
      this.position = coords;
    };
  },
  selectItem(item) {
    if (this.selectedItem) {
      this.selectedItem.element.dataset.state = '';
    }
    this.item[item].element.dataset.state = 'selected';
    this.state = 'selected';
    this.selectedItem = this.item[item];
    this.selectedItem.name = item;
    this.reCountCoords()();
  },

  init() {
    function addProperty(obj, objPropName, addPropName, addProp) {
      if (obj[objPropName]) {
        obj[objPropName][addPropName] = addProp;
      } else {
        obj[objPropName] = {
          [addPropName]: addProp,
        };
      }
    }
    for (let item of Array.from(this.element.children)) {
      if (item.classList[0] === 'menu__item') {
        if (item.classList.length === 2) {
          addProperty(this.item, item.classList[1], 'element', item);
        } else if (item.classList.length === 3) {
          if (item.classList[1] === 'mode') {
            addProperty(this.item, item.classList[2], 'element', item);
          } else if (item.classList[1] === 'tool') {
            if (this.item[item.classList[2].split('-')[0]]) {
              addProperty(this.item, item.classList[2].split('-')[0], 'tool', {
                element: item,
              });
            }
          }
        }
      }
    }
    Object.keys(menuConfig(app, appConfig)).forEach(elem => {
      this.item[elem].element.addEventListener(
        menuConfig(app, appConfig)[elem].eventType,
        menuConfig(app, appConfig)[elem].event
      );
      if (this.item[elem].tool) {
        this.item[elem].tool.element.addEventListener(
          menuConfig(app, appConfig)[elem].tool.eventType,
          menuConfig(app, appConfig)[elem].tool.event
        );
      }
    });
    new Draggable(this.item.drag.element, document.body, coord => (this.position = coord)).init();
    new CustomizableInputTypeFile({
      element: this.item.new.element,
      accept: ['image/jpeg', 'image/png'],
      evtFunc: event => {
        app.err.hideMessage();
        if (event.currentTarget.files.length > 0) {
          app.selectedFile = event.currentTarget.files[0];
          app.serverInt.addImg(app.selectedFile);
        }
      },
    }).init();
    new Dropzone({
      element: this.root,
      accept: ['image/jpeg', 'image/png'],
      evtFunc: event => {
        event.preventDefault();
        let file = event.dataTransfer.files[0];
        if (!app.image) {
          if (['image/jpeg', 'image/png'].includes(file.type)) {
            app.selectedFile = file;
            app.serverInt.addImg(app.selectedFile);
            app.err.hideMessage();
          } else {
            app.err.showMessage({
              msg:
                'Неверный формат файла. Пожалуйста, выберите изображение в формате .jpg или .png.',
              code: 1,
            });
          }
        } else {
          app.err.showMessage({
            msg:
              'Чтобы загрузить новое  изображение воспользуйтесь пунктом "Загрузить новое" в меню',
            code: 1,
          });
        }
      },
    }).init();
    this.positionCenter();
    this.element.addEventListener('click', this.reCountCoords());
    window.addEventListener('resize', this.reCountCoords());
  },
});

export default menu;
