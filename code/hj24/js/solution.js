'use strict';
import Comments from './components/comments.js';
import mask from './module/mask.js';
import menu from './module/menu.js';
import server from './module/serverInt.js';
import error from './components/error.js';
import appConfig from './config/appConfig.js';

(function(app = {}) {
  app.serverInt = server(app, appConfig);
  app.err = error();
  if (appConfig.PARAMETERS.id) {
    app.serverInt.getInfo(appConfig.PARAMETERS.id);
  }
  app.menu = menu(app, appConfig);
  app.menu.init();
  app.mask = mask(app);
  appConfig.imageElement.addEventListener('load', () => appConfig.imgLoader.hide());
  let newComment = event => {
    if (app.menu.selectedItem.name === 'comments' && event.target === app.mask.element.element) {
      new Comments(app).new({
        top: event.clientY,
        left: event.clientX,
      });
    }
  };
  document.addEventListener('click', newComment);
  return app;
})();
